
use std::{
    env,
    path::Path,
    fs::File, 
    io::{BufReader, BufRead}, collections::BTreeMap, time::Instant, sync::mpsc::{self, Sender}, thread,
};

fn read_n_letter_words<P: AsRef<Path>>(path: P, n: usize) -> Vec<String> {
    let mut result = Vec::new();

    let file = File::open(path).unwrap();
    let read = BufReader::new(file);

    for line in read.lines() {
        let line = line.unwrap();
        let line = line.trim_end();

        if line.len() == n {
            result.push(line.to_string());
        }
    }

    result
}

fn to_ord(c: char) -> u8 {
    (c as u8) - ('a' as u8)
}

fn convert_word_to_bitset(word: &str) -> u32 {
    return word.chars()
        .map(|c| to_ord(c))
        .fold(0, |a, b| a | 1 << b);
}

fn convert_to_index(words: Vec<String>) -> BTreeMap<u32, Vec<String>> {
    let mut result = BTreeMap::new();

    for word in words {
        let bitset = convert_word_to_bitset(&word);
        if bitset.count_ones() as usize != word.len() {
            continue;
        }

        if !result.contains_key(&bitset) {
            result.insert(bitset, Vec::new());
        }

        result.get_mut(&bitset).unwrap().push(word);
    }

    result
}

fn prune<'a>(cur: &u32, keys: impl Iterator<Item = &'a u32>) -> Vec<u32> {
    keys.filter(|&k| k > cur && k & cur == 0)
        .map(|k| *k)
        .collect()
}

fn find_n_cliques(n: usize, keys: &Vec<u32>) -> Vec<Vec<u32>> {
    let mut result = Vec::new();
    let mut current = Vec::new();
    find_n_cliques_inner(n, keys, &mut current, &mut result);
    result
}

fn find_n_cliques_inner(n: usize, keys: &Vec<u32>, current: &mut Vec<u32>, result: &mut Vec<Vec<u32>>) {
    if n == 0 {
        result.push(current.clone());
    } else {
        for k in keys.iter() {
            let new_keys = prune(k, keys.iter());

            current.push(*k);
            find_n_cliques_inner(n-1, &new_keys, current, result);
            current.pop();
        }
    }
}

fn find_n_cliques_parallel(n: usize, keys: &Vec<u32>, n_threads: usize) -> Vec<Vec<u32>> {
    if n == 0 {
        return Vec::new();
    }

    let (tx, rx) = mpsc::channel();

    let chunk_size = keys.len() / n_threads;

    thread::scope(|s| {
        for chunk in keys.chunks(chunk_size) {
            let my_tx = tx.clone();
            s.spawn(move || {
                    for k in chunk {
                        let new_keys = prune(k, keys.iter());
                        if n > 1 && new_keys.len() == 0 {
                            continue;
                        }

                        let mut current = vec![*k];
                        find_n_cliques_parallel_inner(n-1, &new_keys, &mut current, &my_tx);
                    }
                });
        }
    });

    drop(tx);

    rx.into_iter().collect()
}

fn find_n_cliques_parallel_inner(n: usize, keys: &Vec<u32>, current: &mut Vec<u32>, tx: &Sender<Vec<u32>>) {
    if n == 0 {
        tx.send(current.clone()).unwrap();
    } else {
        for k in keys.iter() {
            let new_keys = prune(k, keys.iter());

            if n > 1 && new_keys.len() == 0 {
                continue;
            }

            current.push(*k);
            find_n_cliques_parallel_inner(n-1, &new_keys, current, tx);
            current.pop();
        }
    }
}

fn resolve_words<'a> (index: &'a BTreeMap<u32, Vec<String>>, keys: &Vec<u32>) -> Vec<Vec<&'a str>> {
    let mut result = Vec::new();
    let mut words = Vec::new();
    resolve_words_inner(index, keys, &mut words, &mut result);
    result
}

fn resolve_words_inner<'a>(index: &'a BTreeMap<u32, Vec<String>>, keys: &[u32], words: &mut Vec<&'a str>, result: &mut Vec<Vec<&'a str>>) {
    if let Some((head, tail)) = keys.split_first() {
        for w in index.get(head).unwrap() {
            words.push(w);
            resolve_words_inner(index, tail, words, result);
            words.pop();
        }
    } else {
        result.push(words.clone());
    }
}


fn main() {
    let num_threads: usize = env::args().nth(1).map(|v| v.parse().unwrap()).unwrap_or(1);

    if cfg!(feature = "debug_output") {
        println!("running with {num_threads} thread(s)");
    }

    let t_load = Instant::now();
    let words = read_n_letter_words("res/words_alpha.txt", 5);
    let et_load = t_load.elapsed();

    let num_words = words.len();

    let t_create_index = Instant::now();
    let index = convert_to_index(words);
    let keys: Vec<u32> = index.keys().map(|k| *k).collect();
    let et_create_index = t_create_index.elapsed();


    let t_find_n_cliques = Instant::now();
    let cliques = find_n_cliques_parallel( 5, &keys, num_threads);
    let et_find_n_cliques = t_find_n_cliques.elapsed();

    let num_cliques = cliques.len();

    let t_resolve_words = Instant::now();
    let result: Vec<Vec<&str>> = cliques.into_iter().flat_map(|c| resolve_words(&index, &c)).collect();
    let et_resolve_words = t_resolve_words.elapsed();

    let et_e2e = t_load.elapsed();

    if cfg!(feature = "size_output") {
        println!("num words: {num_words}");
        println!("words without perms: {}", index.len());
        println!("solutions without permutations: {num_cliques}");
        println!("solutions with perms: {}", result.len());
        println!();
    }


    if cfg!(feature = "timing_output") {
        println!("load file: {et_load:?}");
        println!("create index: {et_create_index:?}");
        println!("compute without perms: {et_find_n_cliques:?}");
        println!("compute perms: {et_resolve_words:?}");
        println!("e2e: {et_e2e:?}");
        println!();
    }

    if cfg!(feature = "result_output") {
        for tuple in result {
            println!("{}", tuple.join(","));
        }
    }

}
